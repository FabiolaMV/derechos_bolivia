<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'der_bol');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'IzYAHLFmLZ1.FOj=-l){-qv:Bz43EboSZ]`YJeYFq<.+>pM7-S1v!xW+giV@jvI`');
define('SECURE_AUTH_KEY',  'BU(Kw2hHaiqpHvmF`_)Fc` |[6u1~r9LAumJDkB]TgSvTQ&>:o 39*yq;61PJkqN');
define('LOGGED_IN_KEY',    '30.-Q =C7#K*1`uj{qqxu@=eMx6A<.9SvbZ}-`+O2KRgZs ,V}E/%kEZmT1}U6RA');
define('NONCE_KEY',        'Y,9vRyF,x}%4#>59Na96,S2V6wMJeM[H4B2/Br-PrRyt$L!{|huxPK;4<J0>tURg');
define('AUTH_SALT',        'uuJA6o.(WTa~K!kuQomo-4>#+DezFW?V$s~hzp}13u_#x`DHgcGvl=(~nEG@0+-B');
define('SECURE_AUTH_SALT', 'Z^k%KA+^bsoT3]z{>zH.{PtFiVc z5J)gWCRheGu3B}`73k$BFiJO()e`)}QD MB');
define('LOGGED_IN_SALT',   '_R`m|6d,EE4) 5IMC]:*MX/S-vDESYm?`h80xh>e&jx0ZP<tt?o2:QVYJQ}s(BJ/');
define('NONCE_SALT',       'y047gB@,m#do$;<9[+qYEiJv7Y4%bAzLm0fa6)seBJNzPi,l2@/*Xo4`0+!2_t-_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
